from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import math
import os
import random
import sys
import time

import numpy as np
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import seq2seq_model
import data_utils

with tf.Session() as sess:
	meta_ckpt = './Checkpoint/translate.ckpt-34000.meta'
	saver = tf.train.import_meta_graph(meta_ckpt)
	ckpt = './Checkpoint/translate.ckpt-34000'
	saver.restore(sess, ckpt)
	graph = sess.graph
	print([node.name for node in graph.as_graph_def().node])