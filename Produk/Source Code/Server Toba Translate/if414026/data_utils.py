from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gzip
import os
import re
import tarfile
import csv
import tensorflow as tf

from six.moves import urllib

from tensorflow.python.platform import gfile

# Special vocabulary symbols - we always put them at the start.
_PAD = b"_PAD"
_GO = b"_GO"
_EOS = b"_EOS"
_UNK = b"_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3

# Regular expressions used to tokenize.
_WORD_SPLIT = re.compile(b"([.,!?\"':;)(])")
_DIGIT_RE = re.compile(br"\d")

def get_idn_train_set(directory):
  train_path_idn = os.path.join(directory, "/home/itdel/mahasiswa/FilePython/if414026/my_data/NMT/idn_dataset")
  if not (gfile.Exists(train_path_idn +".txt")) :
    print("Tidak ada dataset yang diperlukan")
  return train_path_idn

def get_btk_train_set(directory):
  train_path_btk = os.path.join(directory, "/home/itdel/mahasiswa/FilePython/if414026/my_data/NMT/btk_dataset")
  if not (gfile.Exists(train_path_btk +".txt")) :
    print("Tidak ada dataset yang diperlukan")
  return train_path_btk


def get_idn_dev_set(directory):
  dev_path_idn = os.path.join(directory, "/home/itdel/mahasiswa/FilePython/if414026/my_data/NMT/new_idn")
  if not (gfile.Exists(dev_path_idn + ".txt")):
    print("Tidak ada")
  return dev_path_idn

def get_btk_dev_set(directory):
  dev_path_btk = os.path.join(directory, "/home/itdel/mahasiswa/FilePython/if414026/my_data/NMT/new_btk")
  if not (gfile.Exists(dev_path_btk + ".txt")):
    print("Tidak ada")
  return dev_path_btk


def basic_tokenizer(sentence):
  """Very basic tokenizer: split the sentence into a list of tokens."""
  words = []
  for space_separated_fragment in sentence.strip().split():
    words.extend(_WORD_SPLIT.split(space_separated_fragment))
  return [w for w in words if w]


def create_vocabulary(vocabulary_path, data_path, max_vocabulary_size,
                      tokenizer=None, normalize_digits=True):
  """Create vocabulary file (if it does not exist yet) from data file.

  Data file is assumed to contain one sentence per line. Each sentence is
  tokenized a
    vocabulary_path: path where the vocabulary will be created.
    data_path: data file that will be used to create vocabulary.
    max_vocabulary_size: limit on the size of the created vocabulary.
    tokenizer: a function to use to tokenize each data sentence;
      if None, basic_tokenizer will be used.nd digits are normalized (if normalize_digits is set).
  Vocabulary contains the most-frequent tokens up to max_vocabulary_size.
  We write it to vocabulary_path in a one-token-per-line format, so that later
  token in the first line gets id=0, second line gets id=1, and so on.

  Args:
    normalize_digits: Boolean; if true, all digits are replaced by 0s.
  """

  if not gfile.Exists(vocabulary_path):
    print("Creating vocabulary %s from data %s" % (vocabulary_path, data_path))
    vocab = {}
    with gfile.GFile(data_path, mode="rb") as f:
      counter = 0
      for line in f:
        counter += 1
        line = tf.compat.as_bytes(line)
        if counter % 100000 == 0:
          print("  processing line %d" % counter)
        tokens = tokenizer(line) if tokenizer else basic_tokenizer(line)
        for w in tokens:
          word = _DIGIT_RE.sub(b"0", w) if normalize_digits else w
          if word in vocab:
            vocab[word] += 1
          else:
            vocab[word] = 1
      vocab_list = _START_VOCAB + sorted(vocab, key=vocab.get, reverse=True)
      if len(vocab_list) > max_vocabulary_size:
        vocab_list = vocab_list[:max_vocabulary_size]
      with gfile.GFile(vocabulary_path, mode="wb") as vocab_file:
        for w in vocab_list:
          vocab_file.write(w + b"\n")


def initialize_vocabulary(vocabulary_path):
  """Initialize vocabulary from file.

  We assume the vocabulary is stored one-item-per-line, so a file:
    dog
    cat
  will result in a vocabulary {"dog": 0, "cat": 1}, and this function will
  also return the reversed-vocabulary ["dog", "cat"].

  Args:
    vocabulary_path: path to the file containing the vocabulary.

  Returns:
    a pair: the vocabulary (a dictionary mapping string to integers), and
    the reversed vocabulary (a list, which reverses the vocabulary mapping).

  Raises:
    ValueError: if the provided vocabulary_path does not exist.
  """
  if gfile.Exists(vocabulary_path):
    rev_vocab = []
    with gfile.GFile(vocabulary_path, mode="rb") as f:
      rev_vocab.extend(f.readlines())
    rev_vocab = [line.strip() for line in rev_vocab]
    vocab = dict([(x, y) for (y, x) in enumerate(rev_vocab)])
    return vocab, rev_vocab
  else:
    raise ValueError("Vocabulary file %s not found.", vocabulary_path)


def sentence_to_token_ids(sentence, vocabulary,
                          tokenizer=None, normalize_digits=True):
  """Convert a string to list of integers representing token-ids.

  For example, a sentence "I have a dog" may become tokenized into
  ["I", "have", "a", "dog"] and with vocabulary {"I": 1, "have": 2,
  "a": 4, "dog": 7"} this function will return [1, 2, 4, 7].

  Args:
    sentence: the sentence in bytes format to convert to token-ids.
    vocabulary: a dictionary mapping tokens to integers.
    tokenizer: a function to use to tokenize each sentence;
      if None, basic_tokenizer will be used.
    normalize_digits: Boolean; if true, all digits are replaced by 0s.

  Returns:
    a list of integers, the token-ids for the sentence.
  """

  if tokenizer:
    words = tokenizer(sentence)
  else:
    words = basic_tokenizer(sentence)
  if not normalize_digits:
    return [vocabulary.get(w, UNK_ID) for w in words]
  # Normalize digits by 0 before looking words up in the vocabulary.
  return [vocabulary.get(_DIGIT_RE.sub(b"0", w), UNK_ID) for w in words]


def data_to_token_ids(data_path, target_path, vocabulary_path,
                      tokenizer=None, normalize_digits=True):
  """Tokenize data file and turn into token-ids using given vocabulary file.

  This function loads data line-by-line from data_path, calls the above
  sentence_to_token_ids, and saves the result to target_path. See comment
  for sentence_to_token_ids on the details of token-ids format.

  Args:
    data_path: path to the data file in one-sentence-per-line format.
    target_path: path where the file with token-ids will be created.
    vocabulary_path: path to the vocabulary file.
    tokenizer: a function to use to tokenize each sentence;
      if None, basic_tokenizer will be used.
    normalize_digits: Boolean; if true, all digits are replaced by 0s.
  """
  if not gfile.Exists(target_path):
    print("Tokenizing data in %s" % data_path)
    vocab, _ = initialize_vocabulary(vocabulary_path)
    with gfile.GFile(data_path, mode="rb") as data_file:
      # contents = data_file.read()
      with gfile.GFile(target_path, mode="w") as tokens_file:
        # contents = tokens_file.read()
        counter = 0
        for line in data_file:
          counter += 1
          line = tf.compat.as_bytes(line)
          if counter % 100000 == 0:
            print("  tokenizing line %d" % counter)
          token_ids = sentence_to_token_ids(line, vocab, tokenizer,
                                            normalize_digits)
          tokens_file.write(" ".join([str(tok) for tok in token_ids]) + "\n")


def prepare_data(data_dir, idn_vocabulary_size, btk_vocabulary_size, tokenizer=None):
  """Get data into data_dir, create vocabularies and tokenize data.

  Args:
    data_dir: directory in which the data sets will be stored.
    en_vocabulary_size: size of the English vocabulary to create and use.
    fr_vocabulary_size: size of the French vocabulary to create and use.
    tokenizer: a function to use to tokenize each data sentence;
      if None, basic_tokenizer will be used.

  Returns:
    A tuple of 6 elements:
      (1) path to the token-ids for Indonesia training data-set,
      (2) path to the token-ids for Batak training data-set,
      (3) path to the token-ids for Indonesia development data-set,
      (4) path to the token-ids for Batak development data-set,
      (5) path to the Indonesia vocabulary file,
      (6) path to the Batak vocabulary file.
  """
  # Get data to the specified directory.

  train_path_idn = get_idn_train_set(data_dir)
  train_path_btk = get_btk_train_set(data_dir)
  dev_path_idn = get_idn_dev_set(data_dir)
  dev_path_btk = get_btk_dev_set(data_dir)
  data_dir = '/home/itdel/mahasiswa/FilePython/if414026/my_data/NMT'

  # Create vocabularies of the appropriate sizes.
  idn_vocab_path = os.path.join(data_dir, "vocab%d.idn" % idn_vocabulary_size)
  btk_vocab_path = os.path.join(data_dir, "vocab%d.btk" % btk_vocabulary_size)
  create_vocabulary(idn_vocab_path, train_path_idn + ".txt", idn_vocabulary_size, tokenizer)
  create_vocabulary(btk_vocab_path, train_path_btk + ".txt", btk_vocabulary_size, tokenizer)

  # Create token ids for the training data.
  idn_train_ids_path = train_path_idn + (".ids%d.idn" % idn_vocabulary_size)
  btk_train_ids_path = train_path_btk + (".ids%d.btk" % btk_vocabulary_size)
  data_to_token_ids(train_path_idn + ".txt", idn_train_ids_path, idn_vocab_path, tokenizer)
  data_to_token_ids(train_path_btk + ".txt", btk_train_ids_path, btk_vocab_path, tokenizer)

  # Create token ids for the development data.
  idn_dev_ids_path = dev_path_idn + (".ids%d.idn" % idn_vocabulary_size)
  btk_dev_ids_path = dev_path_btk + (".ids%d.btk" % btk_vocabulary_size)
  data_to_token_ids(dev_path_idn + ".txt", idn_dev_ids_path, idn_vocab_path, tokenizer)
  data_to_token_ids(dev_path_btk + ".txt", btk_dev_ids_path, btk_vocab_path, tokenizer)

  return (idn_train_ids_path, btk_train_ids_path,
          idn_dev_ids_path, btk_dev_ids_path,
          idn_vocab_path, btk_vocab_path)